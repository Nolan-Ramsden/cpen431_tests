## Load Test Client

### Running the Test Client

The test client can be run like any other .jar file:
```
java -jar LoadTest.jar
```

The test client takes an set of options, which can be viewed by running
```
java -jar LoadTest.jar --help
```
which produces: 
Usage: <main class> [options]
  Options:
    -c, --client-threads
       Number of client threads
       Default: 16
    -d, --dont-spread
       Every put is followed by a get to the SAME node rather than random
       Default: false
    --help
       Display this text
       Default: false
    -n, --nodes
       A file containing the nodes in the cluser
       Default: nodes.list
    -p, --payload-size
       How large values should be
       Default: 1000
    -r, --ramp-up
       How long to wait between spinning each thread (secs)
       Default: 0
    -t, --time
       How long to run for in seconds
       Default: 30
```

### Test Client Outputs

The test client prints statistics per-node, as well as the entire cluster stats. Sample output for a single node with 4 clients
is below.

```
[CmdParser] Node Added: localhost:8080
[CmdParser] Nodes (num): 1
[CmdParser] Client Threads: 16
[CmdParser] Test Time (sec): 5
[CmdParser] Payload size (bytes): 1000
[CmdParser] Ramp up spread (sec): 0

Wiping Nodes...Done

Starting up threads 0 seconds apart...
Started thread 16/16
All threads up, collecting stats for 5 seconds...
Stopped thread 16/16s
Checking Nodes Status...Done


== localhost/127.0.0.1:8080 ==
        Status: Alive
   - Counts -
        Total Requests : 2874.0
        Total Timeouts : 0.0
        Total Success : 2874.0
   - Percentages -
        Timeout % : 0.0
        Success % : 100.0
   - Response Times -
        Average : 27.79 ms
        Maximum : 85 ms
        Minimum : 4 ms
   - Throughput Stats -
        Throughput : 574.36 req/sec
        Goodput    : 574.36 req/sec

== NOTABLES ==
        Worst Min Response: localhost/127.0.0.1:8080 (4 ms)
        Worst Max Response: localhost/127.0.0.1:8080 (85 ms)
        Worst Avg Response: localhost/127.0.0.1:8080 (27.79 ms)

== TOTALS ==
   - Counts -
        Total Requests : 2874.0
        Total Timeouts : 0.0
        Total Success : 2874.0
   - Percentages -
        Timeout % : 0.0
        Success % : 100.0
   - Response Times -
        Average : 27.79 ms
        Maximum : 85 ms
        Minimum : 4 ms
   - Throughput Stats -
        Throughput : 574.36 req/sec
        Goodput    : 574.36 req/sec
```